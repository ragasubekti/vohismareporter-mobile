import {Component, ViewChild} from '@angular/core';

import { HomePage } from '../home/home';
import {UserProvider} from '../../providers/user/user';
import {Nav} from 'ionic-angular';
import {ProfilePage} from '../profile/profile';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  @ViewChild(Nav) nav: Nav;

  tab1Root = HomePage;
  // tab2Root = AnnouncementPage;
  tab3Root = ProfilePage;

  constructor(private userProvider: UserProvider) {
  }

  get isUserLogged() {
    return this.userProvider.isUserLogged;
  }
}
