import {Component, ViewChild} from '@angular/core';
import {Content, NavController, NavParams} from 'ionic-angular';
import {UserLoginPage} from '../user-login/user-login';
import {HomePage} from '../home/home';
import {UserProvider} from '../../providers/user/user';
import {SignupActivationPage} from '../signup-activation/signup-activation';

/**
 * Generated class for the UserSignupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-user-signup',
  templateUrl: 'user-signup.html',
})
export class UserSignupPage {

  @ViewChild(Content) content: Content;

  userLogin = UserLoginPage;
  register = {username: '', name: '', email: '', phone: '', password: '', vPassword: '', gender: ''};

  error: string;
  isLoading: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, public userProvider: UserProvider) {
  }

  ionViewWillEnter() {
    if (this.userProvider.isUserLogged) {
      this.navCtrl.setRoot(HomePage)
    }
  }

  private setError(message) {
    this.error = message;
    this.content.scrollToTop();
  }

  onSubmit(formData) {

    this.error = '';
    if (formData.username.length <= 0) {
      this.setError('Username wajib diisi');
    } else if (formData.username.length < 6) {
      this.setError('Panjang minimal username 6 karakter!')
    } else if (formData.name.length <= 0) {
      this.setError('Nama wajib diisi')
    } else if (formData.gender.length <= 0) {
      this.setError('Jenis kelamin wajib dipilih')
    } else if (formData.password.length < 0) {
      this.setError('Password wajib diisi!')
    } else if (formData.password.length < 6) {
      this.setError('Panjang minimal password 6 karakter!')
    } else if (formData.password != formData.vPassword) {
      this.setError('Password tidak cocok!')
    } else if (formData.email.length <= 0) {
      this.setError('E-mail wajib diisi!')
    } else if (formData.phone.length <= 0) {
      this.setError('Nomor Telepon wajib diisi!')
    } else {
      this.isLoading = true;
      this.userProvider.register(formData)
        .then(() => {
          this.isLoading = false;
          this.navCtrl.setRoot(SignupActivationPage);
        })
        .catch(err => {
          this.isLoading = false;
          this.setError(err);
        })
    }
  }

}
