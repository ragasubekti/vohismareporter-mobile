import { Component } from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import { UserSignupPage} from '../user-signup/user-signup';
import {UserProvider} from '../../providers/user/user';
import {HomePage} from '../home/home';
import {TabsPage} from '../tabs/tabs';
import {SignupActivationPage} from '../signup-activation/signup-activation';

/**
 * Generated class for the UserLoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-user-login',
  templateUrl: 'user-login.html',
})
export class UserLoginPage {

  user = {
    username: '',
    password: ''
  };

  error: string;

  signupPage = UserSignupPage;
  signupActivation = SignupActivationPage;

  isSending = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, private userProvider: UserProvider) {
  }

  ionViewWillEnter() {
    if (this.userProvider.isUserLogged) {
      this.navCtrl.setRoot(HomePage)
    }
  }

  isUserNew() {
    if (localStorage.getItem('code')) {
      return true;
    } else {
      return false;
    }
  }

  login() {
    this.error = null;

    if (this.user.username.length <= 0) {
      this.error = 'Username wajib diisi'
    } else if (this.user.password.length <= 0) {
      this.error = 'Password wajib diisi'
    } else {
      this.isSending = true;

      this.userProvider.login(this.user.username, this.user.password)
        .then(result => {
          this.isSending = false;
          this.navCtrl.setRoot(TabsPage, {});

        })
        .catch(err => {
          this.isSending = false;
          this.error = err;
        })
    }
  }

}
