import {Component, ViewChild} from '@angular/core';
import {
  ActionSheetController,
  Content,
  Loading,
  LoadingController,
  NavController,
  NavParams,
  Platform,
  ToastController
} from 'ionic-angular';

import {FilePath} from '@ionic-native/file-path';
import {File} from '@ionic-native/file';
import {Camera} from '@ionic-native/camera'
import {PostProvider} from '../../providers/post/post';

/**
 * Generated class for the PostPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

declare var cordova: any;

@Component({
  selector: 'page-post',
  templateUrl: 'post.html',
})
export class PostPage {
  @ViewChild(Content) content: Content;

  loading: Loading;


  subjectArray = [
    {checked: false, id: 'keluhan'},
    {checked: false, id: 'saran'},
    {checked: false, id: 'informasi'}
  ];

  topicArray = [
    {checked: false, id: 'kesiswaan'},
    {checked: false, id: 'kurikulum'},
    {checked: false, id: 'sarana'},
    {checked: false, id: 'humas'}
  ];

  post = {title: '', location: '', information: '', topic: [], subject: [], media: {type: '', files: []}, public: true};

  error: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public postProvider: PostProvider, private camera: Camera, private file: File, private filePath: FilePath, public actionSheetCtrl: ActionSheetController, public toastCtrl: ToastController, public platform: Platform, public loadingCtrl: LoadingController) {
  }

  imageArray = [];

  onSubmit(formData) {
    this.error = '';
    if (formData.title.length <= 0) {
      this.setError('Judul wajib diisi!');
    } else if (formData.location.length <= 0) {
      this.setError('Lokasi wajib diisi!');
    } else if (formData.subject.length <= 0) {
      this.setError('Subyek wajib dipilih!');
    } else if (formData.topic.length <= 0) {
      this.setError('Topik wajib dipilih!');
    } else {
      formData.files = this.imageArray;

      this.postProvider.post(formData)
        .then(() => {
          this.presentToast('Berhasil terkirim!');
          this.navCtrl.pop();
        })
        .catch(err => {
          this.setError(err);
        });
    }
  }

  capitalize(s) {
    return s && s[0].toUpperCase() + s.slice(1);
  }

  private setError(message) {
    this.error = message;
    this.content.scrollToTop();
  }

  onAddPhoto() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 50,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library

      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {

            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      this.presentToast('Error while selecting image.');
    });
  }

  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

// Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      // this.lastImage = newFileName;
      this.imageArray.push(newFileName);
      console.log(success)
    }, error => {
      this.presentToast('Error while storing file.');
    });
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

// Always get the accurate path to your apps folder
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }


}
