
import {Component, ViewChild} from "@angular/core";
import {Content, NavParams, PopoverController, ToastController, ViewController} from "ionic-angular";
import {UserProvider} from "../../providers/user/user";
import {CommentProvider} from "../../providers/comment/comment";
import {PopoverPage} from "./popover";

@Component({
  templateUrl: './comment.html'
})

export class CommentModal {
  @ViewChild(Content) content: Content;

  data;
  comment;
  commentArray = [];
  error: string;
  isSending = false;
  isLoading = true;
  getError: string;

  constructor(params: NavParams, public viewCtrl: ViewController, public popoverCtrl: PopoverController, public userProvider: UserProvider, public commentProvider: CommentProvider, public toastCtrl: ToastController) {
    this.data = params.data;
  }

  ionViewWillEnter() {
    this.getComment();
  }

  onSubmit(comment) {
    if (comment.length <= 0) {
      this.setError('Komentar tidak boleh kosong!');
    } else {
      this.isSending = true;
      this.commentProvider.post(this.data.id, comment)
        .then((result) => {
          this.isSending = false;
          this.toastCtrl.create({
            message: 'Komentar berhasil dikirim!',
            duration: 3000
          }).present();

          this.comment = '';
          this.getComment();
        })
        .catch((err) => {
          this.isSending = false;
          this.setError(err);
        })
    }
  }

  parseImage(url) {
    const newUrl = url.replace(/['"]+/g, '');
    return 'http://vohismareporter.ml/api/uploads/' + newUrl;
  }

  openMenu($e, row) {
    let popover = this.popoverCtrl.create(PopoverPage, {data: row});
    popover.present({
      ev: $e
    });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  private getComment() {
    this.isLoading = true;
    this.getError = '';
    this.commentProvider.get(this.data.id)
      .then(result => {
        this.isLoading = false;
        this.commentArray = result['data'];
      })
      .catch(err => {
        this.isLoading = false;
        this.getError = err;
      })
  }

  private setError(message) {
    this.error = message;
    this.content.scrollToTop();
  }
}
