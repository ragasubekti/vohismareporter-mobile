import {Component} from '@angular/core';
import {
  ModalController,
  NavController,
  NavParams,
  PopoverController,
  ToastController,
  ViewController
} from 'ionic-angular';
import {UserProvider} from '../../providers/user/user';
import {UserLoginPage} from '../user-login/user-login';
import {PostProvider} from '../../providers/post/post';
import {PostPage} from '../post/post';
import {PopoverPage} from "./popover";
import {CommentModal} from "./comment";
import {ResponseModal} from "./response";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  isLoading = false;
  postArray = [];
  error: string;

  constructor(public navCtrl: NavController, public userProvider: UserProvider, private postProvider: PostProvider, private popoverCtrl: PopoverController, public modalCtrl: ModalController) {
  }

  ionViewWillEnter() {
    if (!this.userProvider.isUserLogged) {
      this.navCtrl.setRoot(UserLoginPage)
    } else {
      this.getPostList()
    }
  }

  parseImage(url) {
    return 'http://vohismareporter.ml/api/upload/get/' + url;
  }

  doRefresh(refresher) {
    this.getPostList();

    setTimeout(() => {
      refresher.complete()
    })
  }

  canResponse(topic) {
    if (this.userProvider.userInfo.level < 20) {
      return true;
    } else {
      for (let i = 0; i < topic.length; i++) {
        if (topic[i].toLowerCase() == 'kurikulum' && this.userProvider.userInfo.level == 21) {
          return true;
        } else if (topic[i].toLowerCase() == 'kesiswaan' && this.userProvider.userInfo.level == 22) {
          return true;
        } else if (topic[i].toLowerCase() == 'sarana' && this.userProvider.userInfo.level == 23) {
          return true;
        } else if (topic[i].toLowerCase() == 'humas' && this.userProvider.userInfo.level == 24) {
          return true;
        }
      }
      return false;
    }
  }

  openMenu($e, row) {
    let popover = this.popoverCtrl.create(PopoverPage, {data: row});
    popover.present({
      ev: $e
    });

    popover.onDidDismiss(() => {
      this.getPostList();
    })
  }

  onCommentClick(data) {
    let modal = this.modalCtrl.create(CommentModal, data);
    modal.present();
  }

  onResponse(data) {
    let modal = this.modalCtrl.create(ResponseModal, data);
    modal.present();
  }

  createPost() {
    this.navCtrl.push(PostPage);
  }

  get isUserLogged() {
    return this.userProvider.isUserLogged;
  }

  private getPostList() {
    this.isLoading = true;
    this.postProvider.list(0, 100)
      .then(result => {
        this.isLoading = false;
        this.postArray = result['data']
      })
      .catch(err => {
        this.isLoading = false;
        this.error = err;
      })
  }

}
