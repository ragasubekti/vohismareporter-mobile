
import {Component, ViewChild} from "@angular/core";
import {Content, LoadingController, NavParams, ToastController, ViewController} from "ionic-angular";
import {ResponseProvider} from "../../providers/response/response";
import {UserProvider} from "../../providers/user/user";

@Component({
  templateUrl: './response.html'
})

export class ResponseModal {

  @ViewChild(Content) content: Content;


  data;
  currentResponse: any;
  response = {type: '', information: ''};
  error: string;

  constructor(params: NavParams, public viewCtrl: ViewController, public resProvider: ResponseProvider, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public userProvider: UserProvider) {
    this.data = params.data;
  }

  ionViewWillEnter() {
    // console.log(this.data)
    this.getResponse();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  onSubmit(formData) {
    this.error = '';
    if (formData.type.length <= 0) {
      this.setError('Mohon pilih tanggapan');
    } else {

      const loading = this.loadingCtrl.create({
        content: "Mohon tunggu"
      });

      loading.present();

      this.resProvider.post(this.data.id, formData.type, formData.information)
        .then(() => {
          loading.dismiss();
          this.toastCtrl.create({
            message: 'Tanggapan berhasil disimpan',
            duration: 3000
          }).present();
          this.viewCtrl.dismiss();
        })
        .catch((err) => {
          loading.dismiss();
          this.setError(err);
        })
    }
  }

  private getResponse() {
    this.resProvider.get(this.data.id)
      .then(result => {
        this.currentResponse = result;
      })
      .catch(err => {
        console.log(err);
      });
  }

  private setError(message) {
    this.error = message;
    this.content.scrollToTop();
  }

}
