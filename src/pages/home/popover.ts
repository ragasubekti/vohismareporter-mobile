

import {Component} from "@angular/core";
import {AlertController, NavParams, ToastController, ViewController} from "ionic-angular";
import {PostProvider} from "../../providers/post/post";

@Component({
  template: `
    <ion-list>
      <!--<button ion-item (click)="close()">-->
        <!--<i class="fa fa-pencil"></i> &nbsp; Ubah-->
      <!--</button>-->
      <button ion-item (click)="delete()">
        <i class="fa fa-trash"></i> &nbsp; Hapus
      </button>
    </ion-list>
  `
})
export class PopoverPage {

  data;

  constructor(public viewCtrl: ViewController, public navParams: NavParams, private postProvider: PostProvider, public alertCtrl: AlertController, public toastCtrl: ToastController) {
    this.data = this.navParams.data.data;
  }

  delete() {
    let alert = this.alertCtrl.create({
      title: 'Konfirmasi Hapus',
      message: 'Apakah anda yakin ingin menghapus kiriman ini?',
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
          handler: () => {
            this.close();
          }
        },
        {
          text: 'Konfirmasi',
          handler: () => {
            this.postProvider.delete(this.data.id)
              .then(result => {
                let toast = this.toastCtrl.create({
                  message: 'Kiriman berhasil dihapus',
                  position: 'top',
                  duration: 3000
                });
                toast.present();
                this.close();
              })
              .catch((err) => {
                this.alertCtrl.create({
                  title: 'Gagal!',
                  message: err,
                  buttons: [{
                    text: 'Close',
                    handler: () => {
                      this.close();
                    }
                  }]
                }).present()
              })
          }
        }
      ]
    });
    alert.present();
  }

  close() {
    this.viewCtrl.dismiss();
  }
}
