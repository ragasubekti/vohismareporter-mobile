import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {UserLoginPage} from '../user-login/user-login';
import {HomePage} from '../home/home';
import {UserProvider} from '../../providers/user/user';

/**
 * Generated class for the SignupActivationPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-signup-activation',
  templateUrl: 'signup-activation.html',
})
export class SignupActivationPage {

  userLogin = UserLoginPage;

  constructor(public navCtrl: NavController, public navParams: NavParams, public userProvider: UserProvider) {
  }

  ionViewWillEnter() {
    if (this.userProvider.isUserLogged) {
      this.navCtrl.setRoot(HomePage)
    } else if (!localStorage.getItem('code')) {
      this.navCtrl.setRoot(UserLoginPage)
    }
  }

  get signupInfo() {
    return {
      code: localStorage.getItem('code'),
      username: localStorage.getItem('username')
    }
  }
}
