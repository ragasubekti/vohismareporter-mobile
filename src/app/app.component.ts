import {Component, ViewChild} from '@angular/core';
import {LoadingController, Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {UserProvider} from '../providers/user/user';
import {UserLoginPage} from '../pages/user-login/user-login';
import {TabsPage} from '../pages/tabs/tabs';
import {AboutPage} from '../pages/about/about';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = TabsPage;
  @ViewChild(Nav) nav: Nav;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public userProvider: UserProvider, private loading: LoadingController) {

    if (!this.userProvider.isUserLogged) {
      this.rootPage = UserLoginPage
    }

    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();

      const notificationOpenedCallback = function (jsonData) {
        console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
      };

      if (window['plugins'] && window['plugins'].OneSignal) {

        window["plugins"].OneSignal
          .startInit("d4884a9d-3ea5-4edb-a8e2-62ecc8ee44e7", "703322744261")
          .handleNotificationOpened(notificationOpenedCallback)
          .endInit();

      }
    });

  }

  openAbout() {
    this.nav.push(AboutPage)
  }

  logout() {
    const loading = this.loading.create({
      content: 'Mohon tunggu',
    });

    loading.present();

    setTimeout(() => {
      loading.dismiss();
      this.userProvider.logout();
      this.nav.setRoot(UserLoginPage);
    }, 3000);
  }
}
