import {NgModule, ErrorHandler} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {MyApp} from './app.component';
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';

import {AboutPage} from '../pages/about/about';
import {ContactPage} from '../pages/contact/contact';
import {HomePage} from '../pages/home/home';
import {TabsPage} from '../pages/tabs/tabs';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {UserLoginPage} from '../pages/user-login/user-login';
import {UserSignupPage} from '../pages/user-signup/user-signup';
import {UserProvider} from '../providers/user/user';
import {CommentProvider} from '../providers/comment/comment';
import {PostProvider} from '../providers/post/post';
import {ProfilePage} from '../pages/profile/profile';
import {PostPage} from '../pages/post/post';

import {File} from '@ionic-native/file';
import {Transfer} from '@ionic-native/transfer';
import {FilePath} from '@ionic-native/file-path';
import {Camera} from '@ionic-native/camera';
import {SignupActivationPage} from '../pages/signup-activation/signup-activation';
import {ResponseProvider} from '../providers/response/response';
import {PopoverPage} from "../pages/home/popover";
import {ResponseModal} from "../pages/home/response";
import {CommentModal} from "../pages/home/comment";

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    UserLoginPage,
    UserSignupPage,
    ProfilePage,
    PostPage,
    PopoverPage,
    ResponseModal,
    CommentModal,
    SignupActivationPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    FormsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    UserLoginPage,
    UserSignupPage,
    ProfilePage,
    PostPage,
    PopoverPage,
    ResponseModal,
    CommentModal,
    SignupActivationPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserProvider,
    CommentProvider,
    PostProvider,
    File,
    Transfer,
    Camera,
    FilePath,
    ResponseProvider
  ]
})
export class AppModule {
}
