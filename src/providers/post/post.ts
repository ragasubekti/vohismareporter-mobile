import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions, URLSearchParams} from '@angular/http';
import 'rxjs/add/operator/map';
import {TransferObject, Transfer} from "@ionic-native/transfer";
import { LoadingController, ToastController } from 'ionic-angular';

declare var cordova: any;

/*
  Generated class for the PostProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class PostProvider {

  public loading;

  constructor(public http: Http, private transfer: Transfer, private loadingCtrl: LoadingController, private toastCtrl: ToastController) {
    console.log('Hello PostProvider Provider');
  }

  list(start, count) {
    return new Promise((resolve, reject) => {
      const url = this.server('post');

      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

      const params = new URLSearchParams();
      params.append('start', start);
      params.append('counts', count);

      const option = new RequestOptions({headers: headers, params: params});

      this.http.get(url, option).subscribe(
        result => {
          if (result.json()['error']) {
            return reject(result.json()['error']);
          } else {
            return resolve(result.json());
          }
        }, err => {
          return reject(err.statusText);
        }
      )
    })
  }

  delete(id) {
    return new Promise((resolve, reject) => {
      const url = this.server('post/' + id);

      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

      const option = new RequestOptions({headers: headers});

      this.http.delete(url, option).subscribe(
        (result) => {
          if (result.json()['error']) {
            return reject(result.json()['error']);
          } else {
            return resolve(result.json())
          }
        },
        (err) => {
          return reject(err.statusText)
        }
      )
    })
  }

  get() {
  }

  post(formData) {
    return new Promise((resolve, reject) => {
      const loading = this.loadingCtrl.create({
        content: 'Mohon tunggu',
      });
      loading.present();

      const url = this.server('post');

      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

      const option = new RequestOptions({headers: headers});
      let media;

      this.uploadImage(formData.files).then((result) => {
        if (!result) {
          media = {
            type: '',
            files: []
          }
        } else {
          // if (result.length > 0) {
          media = {
            type: 'image',
            files: result
          }
        }

        this.http.post(url, {
          title: formData.title,
          location: formData.location,
          topic: formData.topic,
          subject: formData.subject,
          information: formData.information,
          media: media,
          public: formData.public
        }, option).subscribe(result => {
          if (result.json()['error']) {
            loading.dismissAll();
            return reject(result.json()['error']);
          } else {
            loading.dismissAll();
            return resolve(result.json());
          }
        }, err => {
          loading.dismissAll();
          loading.dismissAll();
          return reject(err.statusText);
        })
      });
    })
  }

  public uploadImage(array) {
    return new Promise((resolve, reject) => {
      if (array.length <= 0) return resolve(null);
      let files = [];

      for (let i = 0; i < array.length; i++) {
        var targetPath = this.pathForImage(array[i]);

        // File name only
        var filename = array[i];

        var options = {
          fileKey: "file",
          fileName: filename,
          chunkedMode: false,
          mimeType: "image/jpeg",
          params : {'fileName': filename}
        };

        const fileTransfer: TransferObject = this.transfer.create();



        // Use the FileTransfer to upload the image
        fileTransfer.upload(targetPath, this.server('upload'), options).then(data => {
          this.presentToast('Image succesful uploaded.');
          if (data.response['error']) {
            console.log('error');
            files = [];
            return this.presentToast(data.response['error']);
          } else {
            files.push(data.response.replace(/['"]+/g, ''));
            if (i == array.length - 1) {
              return resolve(files);
            }
          }
        }, err => {
          this.loading.dismissAll();
          this.presentToast('Error while uploading file.');
        });
      }

    });
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  update() {
  }

  private server(page) {
    if (!localStorage.getItem('SERVER_API')) {
      localStorage.setItem('SERVER_API', 'http://vohismareporter.ml/api/');
      return localStorage.getItem('SERVER_API') + page;
    } else {
      return localStorage.getItem('SERVER_API') + page;
    }
  }


  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }


}
