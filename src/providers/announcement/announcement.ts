import { Injectable } from '@angular/core';
import {Headers, Http, RequestOptions, URLSearchParams} from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the AnnouncementProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class AnnouncementProvider {

  constructor(public http: Http) {
    console.log('Hello AnnouncementProvider Provider');
  }

  get() {

  }

  getAll(start, count) {
    return new Promise((resolve, reject) => {
      const url = this.server('announcement');

      const params = new URLSearchParams();
      params.append('start', start);
      params.append('counts', count);

      const headers = new Headers();
      const option = new RequestOptions({headers: headers, params: params});

      this.http.get(url, option).subscribe((result) => {
        if (result.json()['error']) {
          return reject(result.json()['error'])
        } else {
          return resolve(result.json());
        }

      }, err => {
        return reject(err.statusText)
      })
    })
  }

  private server(page) {
    if (!localStorage.getItem('SERVER_API')) {
      localStorage.setItem('SERVER_API', 'http://vohismareporter.ml/api/');
      return localStorage.getItem('SERVER_API') + page;
    } else {
      return localStorage.getItem('SERVER_API') + page;
    }
  }

}
