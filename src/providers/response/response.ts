import { Injectable } from '@angular/core';
import {Headers, Http, RequestOptions, URLSearchParams} from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the ResponseProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ResponseProvider {

  constructor(public http: Http) {
    console.log('Hello ResponseProvider Provider');
  }

  post(id, type, information) {
    return new Promise((resolve, reject) => {
      const url = this.server('response');

      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

      const option = new RequestOptions({headers: headers});

      this.http.post(url, {
        id: id,
        type: type,
        information: information
      }, option).subscribe((result) => {
        if(result.json()['error']) {
          return reject(result.json()['error']);
        } else {
          return resolve(result.json());
        }
      }, (err) => {
        return reject(err.statusText);
      })
    })
  }

  get(id) {
    return new Promise((resolve, reject) => {
      const url = this.server('response');

      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

      const params = new URLSearchParams();
      params.append('id', id);

      const option = new RequestOptions({headers: headers, params: params});

      this.http.get(url, option).subscribe(
        result => {
          if (result.json()['error']) {
            return reject(result.json()['error']);
          } else {
            return resolve(result.json());
          }
        }, err => {
          return reject(err.statusText);
        }
      )
    })
  }

  private server(page) {
    if (!localStorage.getItem('SERVER_API')) {
      localStorage.setItem('SERVER_API', 'http://vohismareporter.ml/api/');
      return localStorage.getItem('SERVER_API') + page;
    } else {
      return localStorage.getItem('SERVER_API') + page;
    }
  }
}
