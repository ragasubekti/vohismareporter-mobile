import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class UserProvider {

  constructor(public http: Http) {
  }

  get isUserLogged() {
    const token = localStorage.getItem('token');
    if (token) {
      return true;
    } else {
      return false;
    }
  }

  /**
   *
   * @param username
   * @param password
   * @returns {Promise<any>}
   */
  login(username, password): Promise<any> {
    return new Promise((resolve, reject) => {
      const url = this.server('authentication');

      this.http.post(url, {
        username: username,
        password: password
      }).subscribe(result => {
        if (result.json()['error']) {
          return reject(result.json()['error']);
        } else {

          if (localStorage.getItem('code')) {
            localStorage.removeItem('code');
            localStorage.removeItem('username');
          }

          const userData = result.json()['data'];

          if (window['plugins'] && window['plugins'].OneSignal) {

            if (parseInt(userData['user_level']) == 21) {
              window['plugins'].OneSignal.sendTag('waka', 'kurikulum');
            } else if (parseInt(userData['user_level']) == 22) {
              window['plugins'].OneSignal.sendTag('waka', 'kesiswaan');
            } else if (parseInt(userData['user_level']) == 23) {
              window['plugins'].OneSignal.sendTag('waka', 'sarana');
            } else if (parseInt(userData['user_level']) == 24) {
              window['plugins'].OneSignal.sendTag('waka', 'humas')
            }

          }

          localStorage.setItem('token', result.json()['token']);
          localStorage.setItem('user_data', JSON.stringify(userData));

          return resolve(result.json());
        }
      }, err => {
        return reject(err.statusText);
      })
    })
  }

  register(formData): Promise<any> {
    return new Promise((resolve, reject) => {
      const url = this.server('signup');

      this.http.post(url, {
        username: formData.username,
        password: formData.password,
        name: formData.name,
        email: formData.email,
        phone: formData.phone
      }).subscribe(result => {
        if (result.json()['error']) {
          return reject(result.json()['error']);
        } else {
          localStorage.setItem('username', formData.username);
          localStorage.setItem('code', result.json()['activation_code']);
          return resolve(result.json());
        }
      }, err => {
        if (err.status == 0) err.statusText = 'Connection timed out';
        return reject(err.statusText);
      })
    })
  }

  get userInfo(): any | boolean {
    if (this.isUserLogged) {
      if (localStorage.getItem('user_data')) {
        const user = JSON.parse(localStorage.getItem('user_data'));
        return {
          username: user.username,
          name: user.full_name,
          level: user.user_level,
          email: user.email,
          phone: user.phone,
          password: null,
          vPassword: null
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  logout() {
    localStorage.removeItem('user_data');
    localStorage.removeItem('token');
    window['plugins'].OneSignal.deleteTag('waka');
  }

  private server(page) {
    if (!localStorage.getItem('SERVER_API')) {
      localStorage.setItem('SERVER_API', 'http://vohismareporter.ml/api/');
      return localStorage.getItem('SERVER_API') + page;
    } else {
      return localStorage.getItem('SERVER_API') + page;
    }
  }

}
